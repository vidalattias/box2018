\documentclass[]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[a4paper]{geometry}
\usepackage[french]{babel}
\usepackage{csquotes}
\usepackage{xcolor}
\usepackage{colortbl}
\usepackage{multirow}
\usepackage{listings}
\usepackage{graphics}
\definecolor{light-gray}{gray}{0.95}
\usepackage[framemethod=tikz]{mdframed}
\lstset{
	basicstyle=\ttfamily\footnotesize,
	columns=fullflexible,
	keepspaces=true,
	captionpos=b,
	language=C,
	breaklines=true,
}
\surroundwithmdframed[
hidealllines=true,
backgroundcolor=light-gray,
innerleftmargin=0pt,
innertopmargin=0pt,
innerbottommargin=0pt]{lstlisting}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\newcommand{\var}[1]{\lstinline|#1|}
\newcommand{\relec}[1]{[{\color{red} #1}]}
\newcommand{\note}[1]{[{\color{blue} \tiny #1}]}

%opening
\title{BOX -- Rapport utilisateur}
\author{Vidal Attias \and Dylan Marinho}
\date{10 Décembre 2018}

\begin{document}

\maketitle

\section{Introduction}
Le présent rapport présente l'utilisation du logiciel de compression/décompression de séquençages ADN par Vidal Attias et Dylan Marinho. L'objectif étant de produire à partir d'un ensemble de séquences que nous appellerons des \emph{reads}, un fichier de compression ainsi qu'un fichier de graph de de Bruijn permettant la reconstruction des reads originaux, dont la somme des tailles en bit est la plus petite possible. Nous proposons une implémentation fonctionnant avec succès avec une petite extension.

\subsection{Choix algorithmiques}
Nous avons choisi de nous en tenir strictement aux consignes données dans le sujet, sans faire les bonus proposés. Une partie intéressante dans notre implémentation est de considérer le fichier de compression comme un flux (\emph{stream}) continu de caractères, que nous utiliserons pour reconstruire les reads originaux. Cette approche nous permet de ne pas écrire de séparateurs entre les reads et donc d'économiser une place précieuse. Il suffit de commencer la lecture d'un nouveau read lorsque le read courrant a une taille égale à celle d'un read original.

Nous avons par ailleurs fait le choix de ne pas implémenter le \emph{reverse complementing} pour des erreurs se produisant au mileu de reads pour des questions de simplicité. Nous perdons donc en taux de compression.


\section{Cadre d'utilisation}
\subsection{Lancer le programme}

Notre logiciel s'utilise de façon assez simple. Lorsque l'on se situe dans le dossier racine, il se lance via un terminal \emph{bash} avec la commande suivante et ses options :

\begin{lstlisting}
	python src/main.py [options]
\end{lstlisting}

Les options peuvent être écrites dans un ordre quelconque, et sont de la forme :

\begin{itemize}
	\item \var{-k <int>} : la taille des \emph{k}-mers
	\item \var{-m <int>} : la taille du filtre de Bloom en bits
	\item \var{-t <int>} : le seuil de solidité des \emph{k}-mers
	\item \var{--file <string>} : l'identifiant du fichier de séquençage à compresser, localisé dans le dossier \var{fasta/}. L'identifiant correspond à la partie suffixant le \var{.comp}.
	\item \var{--histogram} : active l'affichage d'un histogramme des \emph{k}-mers distincts. Si non présent, l'histogramme ne sera pas affiché.
\end{itemize}

\begin{lstlisting}[
	caption=Exemple d'utilisation de la commande,
	label=lst:exampleUse,
	]
	python src/main.py -k 32 -m 1000000 -t 2 --file mysterious --histogram
\end{lstlisting}

La commande présentée par le Listing \ref{lst:exampleUse} compresse le fichier \var{fasta/mysterious.fasta} avec des $k$-mers d'une taille de 32, un filtre de Bloom de 1 000 000 bits, un seuil de solidité des $k$-mers de 2 en activant l'affichage de l'histogramme.

\subsection{Résultats de l'algorithme}
L'algorithme va écrire le fichier compressé \var{identifiant.comp} et le fichier du graphe de de Bruijn \var{identifiant.graph.pgz} dans le répertoire \var{result}. Il va ensuite décompresser les fichiers obtenus dans le répertoire \var{uncompressed/} sous le nom \var{identifiant.fasta}. De plus en fin d'exécution, le programme affiche un message indiquant si la compression/décompression s'est bien effectuée en comparant le fichier d'origine et le fichier décompressé.


\section{Évaluation des performances}
Après réalisation de l'algorithme, nous avons décidé de mener des expériences sur l'efficacité de notre logiciel en variant les différents paramètres. Bien que le sujet demande une étude sur le jeu de données \emph{mysterious}, nous avons été obligés de mener nos expériences sur le jeu de données \emph{Reads erronés (1\% d'erreur) issus du fragment de génome de 25kb d'E. coli} car mener les compressions sur ce nombre de points était beaucoup trop long et nous avions des problèmes dûs à un changement d'ordinateur. Nous avons cependant pu effectuer certaines compressions sur le jeu de données \emph{mysterious} que nous présenterons plus loin.

\subsection{Variation en fonction de $k$ avec $m$ fixé}
Une première question que l'on peut se poser en manipulant ce logiciel est l'influence de ses paramètres $k$, $m$ et $t$ sur la compression. Nous avons donc commencé par étudier celui qui présentait pour nous l'influence la plus grande, à savoir $k$. Nous avons donc étudié trois cas. Le premier, en prenant une valeur de $m$ ridiculement petite afin d'observer le comportement en cas de remplissage du filtre de Bloom, une seconde avec une valeur immense de $m$ et enfin une valeur que l'on qualifiera de "raisonnable".

\subsubsection{Très petite valeur de $m$}
Nous avons pris une valeur très petite de $m$, à savoir 1000. Nous obtenons les résultats dans la figure \ref{fig:toutPetit}


%Version 2 images - 1 titre
\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/toutpetit.png}
	\caption[Variation de la taille des fichiers]{Variation de la taille des fichiers en fonction de $k$, pour $m=1000$ et $t=3$}
	\label{fig:toutPetit}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/petit_taux.png}
	\caption[Variation de la taille des fichiers]{Variation du taux de compression des fichiers en fonction de $k$, pour $m=1000$ et $t=3$}
	\label{fig:toutPetit2}
\end{figure}
On observe un résultat assez prévisible, étant donné que le filtre est plein, tous les chemins existent dans le filtre, ce qui résulte en une écriture de tous les reads en entier et donc une compression nulle. On en déduit donc que la valeur de $m$ ne doit pas être choisie trop bas pour éviter les non-compressions. On observe tout de même une compression de l'ordre de 60\%, mais il s'agit en réalité des \emph{headers} que l'on retire et éventuellement des retours à la ligne supprimés.

\subsubsection{Très grande valeur de $m$}
On a ensuite pris une valeur très grande de $m$, à savoir 10,000,000,000, afin de voir l'influence sur le taux de compression. Nous avons comparés sur la figure \ref{fig:tresGrand} avec une valeur toute aussi grande mais déjà moins de $m$, 10,000,000.

%Version 2 images - 2 titres
\begin{figure}[h]
		\includegraphics[width=0.95\linewidth]{figures/taux_grand.png}
		\caption[Variation de la taille des fichiers]{Taux de compression $m=10,000,000$}
		\label{fig:tresGrand}
\end{figure}
	\hfill
	\begin{figure}[h]
		\includegraphics[width=0.95\linewidth]{figures/taux_grandgrand.png}
		\caption[Variation du taux de compression des fichiers]{Taux de compression $m=10,000,000,000$}
		\label{}
	\end{figure}
On observe exactement la même courbe, notre explication est qu'il existe une valeur de $m$ à partir de laquelle augmenter $m$ ne réduit plus nécessairement le nombre de collisions, ainsi l'influence de $m$ devient nulle sur la compression et ne fait qu'augmenter la taille occupée par le fichier du graphe.

\subsubsection{Pour une valeur raisonnable de $m$}
Nous avons essayé une valeur que nous jugions raisonnable de $m$, à savoir 1,000,000. Ici commence donc notre étude sur l'influence de $k$ à proprement dite. Nos reads ayant une longueur de 100 caractères, nous avons donc échantillonné nos compressions sur des valeurs de $k$ de 0 à 90, avec un pas de 10. Nous obtenons la courbe de la figure \ref{fig:raisonnable}.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/raisonnable.png}
	\caption[Variation de la taille des fichiers]{Variation de la taille des fichiers en fonction de $k$, pour $m=1,000,000$ et $t=3$}
	\label{fig:raisonnable}
\end{figure}

On observe ici une compression assez efficace, de l'ordre de 15\% à son minimum, mais qui dépend très fortement de la valeur de $k$. En effet, on observe une décroissance rapide au début, qui s'explique selon nous par la présence au début d'un grand nombre de $k$-mer distincts à cause de la taille de $k$, ce qui a pour effet de remplir rapidement le filtre de Bloom, et ensuite une augmentation linéaire, selon nous dûe au fait que les reads ont une taille minimale de $k$ caractères, et pour peu que l'on ait un graphe sans trop de branchements, cet effet d'augmentation linéaire se ressent très fortement.

\subsection{Variation en fonction de $m$}
Nous avons vu dans la partie précédente que l'on pouvait trouver, en tout cas pour notre jeu de données, une valeur optimale de $k$. Nous avons pris ici $k=20$. Nous étions alors intéressés à étudier l'influence de $m$ sur la compression. Nous avons donc tracé la courbe correspondant, dans la figure \ref{fig:var_m}.



\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/m1.png}
	\caption[Variation de la taille des fichiers]{Variation de la taille des fichiers en fonction de $m$, pour $k=20$ et $t=3$}
	\label{fig:var_m}
\end{figure}


Nous avons volontairement poussé la valeur de $m$ à des valeurs \emph{trop} élevées, pour mettre en lumière un phénomène important à avoir à l'esprit lors de l'utilisation de notre logiciel : la valeur de $m$ est très importante car l'on peut être tenté de mettre une valeur très grande pour éviter les collisions dans le filtre de Bloom, cependant il ne faut pas oublier qu'à la taille du fichier compressé s'ajoute la taille du fichier du graphe. Ici on observe que pour une valeur trop grande on a même le poids des deux fichiers qui dépasse le poids du fichier original, ce qui est pire encore qu'absurde, c'est contre-productif.
\par Nous observons ici encore une fois un minimum, en fonction de la valeur de $m$, du poids de deux fichiers, à l'instar de $k$. Certes le poids du fichier compressé diminue pour atteindre un poids stable, mais le poids du fichier de graphe augmente linéairement avec $m$.

\subsubsection{Variation en fonction de $t$}
Ayant touvé deux minimum pour $m$ et $k$, nous avons voulu tester l'influence du seuil de solidité des $k$-mers. Pour cela, nous avons fixé $k=20$ et $m=1,000,000$ et nous avons fait varier $t$ de 1 à 9 avec un pas de 1. Nous obtenons donc la figure \ref{fig:t}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/t.png}
	\caption[Variation de la taille des fichiers]{Variation de la taille des fichiers en fonction de $m$, pour $k=20$ et $t=3$}
	\label{fig:t}
\end{figure}


Nous observons une croissance de la taille du fichier compressé avec $t$. Nous expliquons cela par le fait que les reads erronés ne sont pas compressés et donc écrits en entiers, ce qui rajoute du poids à la compression. Nous n'observons même pas de dégression avec l'augmentation de $t$ une fois passé les premiers rejets de $k$-mers, nous avons donc beaucoup de mal à comprendre l'intérêt de cette fonction.

\subsection{Temps d'exécution}
Nous avons réuni les temps d'exécution pour différentes valeurs de $k$, avec $m=10,000,000,000$ et $t=3$. On divise l'exécution en trois temps principaux, la construction du graphe, avec le découpage en $k$-mers et l'insertion dans le filtre de Bloom, le temps de compression à proprement parler et le temps de décompression. On peut observer ces temps dans la figure \ref{fig:temps}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/temps_complet.png}
	\caption[Variation du temps d'exécution du logiciel]{Variation du temps d'exécution du logiciel}
	\label{fig:temps}
\end{figure}

On note que le temps de construction du graphe est assez court par rapport à la compression/décompression mais pas non plus négligeable. On observe surtout qu'on a une relation linéairement décroissante entre le temps de compression et la taille des $k$-mers, ce qui est assez embêtant par rapport à notre propriété précédente selon laquelle plus $k$ est bas meilleure est la compression. Il y a donc un compromis à faire entre le taux de compression et le temps d'exécution.

\section{Conclusion}

Nous avons réalisé un compresseur--décompresseur pour le séquençage ADN. Le principal choix algorithmique a été de considérer le fichier de compression comme un flux de données, ce qui permet d'économiser encore plus d'espace mémoire.

Afin de déterminer les paramètres optimaux, plusieurs simulations ont été effectuées. Il en résulte qu'un bon choix de $k$ se trouve autour de 20 (minimum obtenu en variant $m$). Pour $m$, ce choix doit être judicieusement fait: s'il est trop petit le filtre sera plein, s'il est trop grand, la taille du graphe sera grande. Enfin, pour le seuil de solidité des k-mers, notre étude n'a pas su donner de résultats exploitables.

Notre dernière analyse a porté sur le temps d'exécution. Celui-ci est plus important lorsque $k$ est faible. Il faut donc savoir trouver un compromis entre temps et taux de compression.
\end{document}
