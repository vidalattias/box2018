\documentclass[]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[a4paper]{geometry}
\usepackage[french]{babel}
\usepackage{csquotes}
\usepackage{xcolor}
\usepackage{colortbl}
\usepackage{multirow}
\usepackage{listings}
\definecolor{light-gray}{gray}{0.95}
\usepackage[framemethod=tikz]{mdframed}
\lstset{
	basicstyle=\ttfamily\footnotesize,
	columns=fullflexible,
	keepspaces=true,
	captionpos=b,
	language=C,
	breaklines=true,
}
\surroundwithmdframed[
hidealllines=true,
backgroundcolor=light-gray,
innerleftmargin=0pt,
innertopmargin=0pt,
innerbottommargin=0pt]{lstlisting}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\newcommand{\var}[1]{\lstinline|#1|}
\newcommand{\relec}[1]{[{\color{red} #1}]}
\newcommand{\note}[1]{[{\color{blue} \tiny #1}]}

%opening
\title{BOX -- Rapport développeur}
\author{Vidal Attias \and Dylan Marinho}

\begin{document}

\maketitle

\section{Objectif du logiciel}

	\subsection{Cahier des charges}
	L'objectif est d'implémenter un compresseur--décompresseur pour des lectures de séquençage ADN.

	\begin{description}
		\item[Compresseur] À partir d'un fichier au format \emph{fasta}, fournir une version compressée du Graphe de de Bruijn et une version compressée de l'entrée.

		\item[Décompresseur] À partir des deux fichiers compressés, on veut retrouver les données du fichier initial.
	\end{description}

	\paragraph{Remarque.} On ne conservera pas les \emph{headers} du fichier initial. De plus, on ne garantit pas que l'ordre des \emph{éléments} sera conservé.


	\subsection{Description des formats}

	Le format \enquote{\emph{fasta}} utilise le caractère \var{>} pour commencer une ligne de commentaires. Chaque fichier présente des \emph{reads} par une ligne de commentaire et une ligne représentant la lecture des nucléotides (représentés par A, T, C et G). Le \emph{Listing} \ref{listing:fasta} présente un extrait d'un fichier fasta (les reads présentés ayant été tronqués).

	\begin{lstlisting}[
		caption=Extrait du fichier mysterious\_perfect\_reads.fasta,
		label=listing:fasta,
		basicstyle=\footnotesize\ttfamily
	]
	>read0_contig0_position13884_M0_I0_D0_NG0______er0__indel0__rgeom0_rev0
	TAAAGCCCATACTCAGTCTGTTCTAAACGGCTTCCATGGAAGAAAAATAACTATATTATAGCTTTCCT...
	>read1_contig0_position5438_M0_I0_D0_NG0______er0__indel0__rgeom0_rev0
	AGAAAACAGACCCATTCACCCACGACGTATCAAGTTACTTCCTTGGTGCAATGTCCCACTATAAAAAA...
	\end{lstlisting}

	Le format \enquote{\emph{graph.pgz}}, permettant de stocker la version compressée du graphe de de Bruijn. Celui-ci est stocké grâce au module \var{pickle} de Python, permettant de stocker et récupérer \emph{rapidement} l'objet.

	Le format \enquote{\emph{comp}}, permettant de stocker la version compressée de l'entrée, \relec{....}

\section{Vue générale du fonctionnement}

	Le code est arrangé autour de deux classes principales, \var{Compressor} et \var{Uncompressor}, représentant respectivement les méthodes du compresseur et du décompresseur. D'autres classes, telles que \var{BloomFilter} et \var{Kmer2hash}, permettent une meilleure organisation du code. Nous avons décidé de toujours conserver l'ordre original des reads pour des soucis de simplicité et d'efficacité.

	\subsection{Compressor}

		La classe \var{Compressor} permet de créer un compresseur de la donnée en entrée. À partir de la taille des k-mers et de l'entrée, on définit un filtre de Bloom.

		Le compressor fonctionne en deux étapes. Il commence par la méthode \var{count_kmer} qui s'occupe de compter les kmers existants, construire le filtre de Bloom et éventuellement afficher un histogramme des fréquences d'apparition des kmers distincts.

		Dans un second temps, il s'occupe de la compression read par read, sans mettre de séparateur entre eux. Dans le cas de chemin ambigu, nous avons décidé de ne pas implémenter de correction de compression et de simplement écrire le read en entier.



		\begin{description}
			\item \var{next\_character} permet d'obtenir, pour un k-mer, les caractères permettant de compléter les reads en entrée. Il est important de noter que ce caractère peut éventuellement être le caractère nul ($\varepsilon$).
			\item \var{compress} permet de compresser l'ensemble des reads. Les résultats sont données sous les fichiers \var{compression.txt}, \var{.comp} et \var{.graph.pzg}.
		\end{description}
		Il est à noter que dans le fichier \var{.graph.pgz}, on sauvegarde également la valeur de \var{k}, la taille des reads que l'on suppose constante et le nombre de reads.

	\subsection{Uncompressor}

		La classe \var{Uncompressor} permet de créer un décompresseur d'un ensemble de read compressés. L'initialisation permet de récupérer les différentes données fournies par le compresseur (fichiers \var{.comp} et \var{.graph.pgz}).

		\begin{description}
			\item \var{find\_next\_kmer} permet de chercher, à partir du graphe de de Bruijn, le k-mer suivant l'entrée.
			\item \var{reconstruct\_read} permet, à partir de la méthode précédente, de reconstruire un read.
			\item \var{uncompress} permet la décompression. Le résultat est renvoyé sous la forme d'un fichier \var{fasta}.
		\end{description}

		Il est à noter que pour la classe \var{Uncompressor}, le fichier \var{.comp} est considéré comme un \emph{stream} de caractères dans
	${A,C,G,T,@}$ et que la méthode \var{find\_next\_kmer} "consomme" un caractère. Cette vision nous a permis une programmation plus efficace car moins sujette à des erreurs d'indice, mais surtout pour économiser la place prise par les \var{\\n}. En effet, on arrête de décompresser un read lorsque la taille des reads est atteinte.

	\subsection{Les autres classes}

		\paragraph{BloomFilter.} Cette classe permet de créer et gérer un filtre de Bloom. Elle fonctionne autour de trois méthodes:
			\begin{description}
				\item \var{init} qui crée un filtre de Bloom à partir de la taille souhaitée;
				\item \var{add\_word} qui met à jour un \var{BloomFilter} en y ajoutant un mot;
				\item \var{request} qui vérifie si un mot a été trouvé (au sens du filtre) ou non.
			\end{description}

		\paragraph{Comparator.} Cette classe permet de comparer la version originale et la version compressée-décompressée obtenue par notre programme en lisant simplement dans l'ordre les reads du fichier original et du fichier décompressé.

\end{document}
