class Comparator:
    def __init__(self, file_name):
        file = open("fasta/"+file_name+".fasta")
        self.original = []
        for line in file:
            if line[0] != ">":
                self.original.append(line.replace("\n",""))

        file = open("uncompressed/"+file_name+".fasta")
        self.uncompressed = []
        for line in file:
            self.uncompressed.append(line.replace("\n",""))

    def compare(self):
        print("\n\n###############")
        print("Comparing the compressed and uncompressed data")
        n = len(self.original)
        false = 0
        for i in range(n):
            if self.original[i] != self.uncompressed[i]:
                false += 1
                print(False)
        print("Amount of false:")
        print(str(false)+"/"+str(n)+" = "+str(false/n*100)+"%")
