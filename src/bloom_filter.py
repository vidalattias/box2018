from bit_array import BitArray
import Kmer2Hash

class BloomFilter:
    def __init__(self, _array_size, _number_hash_functions=7):
        print(Kmer2Hash.kmer2hash)
        self.array = BitArray(_array_size)
        self.H = _number_hash_functions
        self.size = _array_size

    def add_word(self, _word):
        for hash_index in range(self.H):
            index = Kmer2Hash.kmer2hash(_word, hash_index)%self.size
            self.array.set_element(index, 1)

    def request(self, _word):
        exists = True

        for hash_index in range(self.H):
            index = Kmer2Hash.kmer2hash(_word, hash_index)%self.size
            if self.array.retrieve_element(index) == 0:
                exists = False
        return exists
