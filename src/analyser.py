import os
import matplotlib.pyplot as plt


original_size = os.path.getsize('fasta/best.fasta')

kmer = [0,10,20,30,40,50,60,70,80,90]
file = 'best_e'
threshold = 3
m = 10000000


plt.cla()
sizes = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+'.0.comp')/original_size for k in kmer]

sizes_graph = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+'.0.graph.pgz')/original_size for k in kmer]

sizes_original = [original_size/original_size for k in kmer]

sizes_sum = []

for i in range(len(sizes)):
    sizes_sum.append(sizes[i]+sizes_graph[i])

plt.plot(kmer, sizes, label="10000000")
























m = 10000000
file = 'best_e'

sizes = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+'.0.comp')/original_size for k in kmer]

sizes_graph = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+'.0.graph.pgz')/original_size for k in kmer]

sizes_original = [original_size/original_size for k in kmer]

sizes_sum = []

for i in range(len(sizes)):
    sizes_sum.append(sizes[i]+sizes_graph[i])




























plt.plot(kmer, sizes_original, label="Original")

plt.legend()
plt.xlabel("Valeur de K")
plt.ylabel("Taille des fichiers")
plt.ylim([0,1.1])
plt.title("Variation du taux de compression des fichiers compressés en fonction de K, avec M="+str(m)+" et T="+str(threshold))
plt.savefig("first.png")
plt.show()

'''
original_size = os.path.getsize('fasta/best.fasta')

file = 'best_e'
k = 20
thresholds = [i for i in range(11)]
m = 1000000

plt.cla()
sizes = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+"."+str(t)+'.comp') for t in thresholds]

sizes_graph = [os.path.getsize('result/'+file+"."+str(k)+"."+str(m)+"."+str(t)+'.graph.pgz') for t in thresholds]

sizes_original = [original_size for t in thresholds]

sizes_sum = []

for i in range(len(sizes)):
    sizes_sum.append(sizes[i]+sizes_graph[i])

plt.plot(thresholds, sizes, label="Comp")
plt.plot(thresholds, sizes_graph, label="Graphe")
plt.plot(thresholds, sizes_sum, label="Some")
plt.plot(thresholds, sizes_original, label="Original")

plt.legend()
plt.xlabel("Valeur de M")
plt.ylabel("Taille des fichiers")
plt.title("Variation de la taille des fichiers compressés en fonction de T, avec K="+str(k)+" et M="+str(m))
plt.savefig("first.png")
plt.show()
'''
