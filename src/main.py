import matplotlib.pyplot as plt
import os
from compressor import Compressor
from uncompressor import Uncompressor
from comparator import Comparator

kmer = [0,10,20,30,40,50,60,70,80,90]

file = "best_e"
k = 20
threshold = 0
m = 10000000

for k in kmer:
    plt.cla();
    print("#####################################")
    print("NEW SIMULATION WITH K = "+str(k)+" AND M = "+str(m)+" AND THRESHOLD = "+str(threshold))


    #First argument : file_name
    #Second argument : filter_size
    #Third argument : k-mer size
    #Fourth argument : k-mer threshold
    compressor = Compressor(file, m, k, threshold)
    compressor.count_kmer()
    compressor.compress()
