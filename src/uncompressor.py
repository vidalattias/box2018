from progressbar import ProgressBar
import pickle
import time

class Uncompressor:
    def __init__(self, file, k = 0,filter_size = 0, threshold = 0):
        pickled = pickle.load(open("result/"+file+"."+str(k)+"."+str(filter_size)+"."+str(threshold)+".graph.pgz", "rb"))
        self.file_name = file
        self.threshold = threshold
        self.filter_size = filter_size
        self.filter = pickled['filter']
        self.kmer_size = pickled['kmer_size']
        self.reads_size = pickled['read_size']
        self.nb_reads = pickled['nb_reads']
        self.long_file_name = file+"."+str(self.kmer_size)+"."+str(self.filter_size)+"."+str(self.threshold)

        self.uncompress_time = 0

        print(self.kmer_size)
        print(self.reads_size)


        self.originals = []
        file = open("fasta/best.fasta")
        for line in file:
            if line[0] != ">":
                self.originals.append(line.replace("\n",""))

        self.reads = []
        self.file = open("result/"+self.long_file_name+".comp").read()+"E"

    def consume(self, length):
        n = len(self.file)
        return_value = self.file[:length]
        self.file = self.file[length:]

        return return_value

    def find_next_kmer(self, prefixe_kmer, next_branch):
        number_branches = 0
        if self.filter.request(prefixe_kmer+"A"):
            number_branches += 1
            character = "A"
        if self.filter.request(prefixe_kmer+"C"):
            number_branches += 1
            character = "C"
        if self.filter.request(prefixe_kmer+"G"):
            number_branches += 1
            character = "G"
        if self.filter.request(prefixe_kmer+"T"):
            number_branches += 1
            character = "T"

        if number_branches != 1:
            return (True, next_branch)
        else:
            return (False, character)

    def reconstruct_read(self, bool=False):
        k = self.kmer_size
        read = self.consume(k)
        new_read = "" + read
        position = 0

        while len(new_read) < self.reads_size:
            next_char = self.find_next_kmer("" + new_read[1+position:position+k], self.file[0])
            if next_char[0] == True:
                self.consume(1)
            new_read += next_char[1]
            position += 1
        return new_read

    def uncompress(self):

        self.new_reads = []
        j = 0
        content = ""

        progress = ProgressBar(self.nb_reads)

        t1 = time.time()

        i = 0

        while len(self.file) > 1:
            progress.print(j)
            if self.file[0] == "@":
                self.consume(1)
                read = self.consume(self.reads_size)
            else:
                if 0:
                    read = self.reconstruct_read(True)
                else:
                    read = self.reconstruct_read()

            self.new_reads.append(read)
            content += read+"\n"
            j+=1
            i += 1

        print("")

        self.uncompress_time = time.time() - t1
        print("Uncompressing time : "+str(self.uncompress_time))


        file = open(self.file_name +"_uncompression.txt", 'a')
        file.write( str(self.kmer_size)+";"+str(self.filter_size)+";"+str(self.threshold)+";"+str(self.uncompress_time)+"\n")
        file.close()


        file = open("uncompressed/"+self.long_file_name+".fasta", "w")
        file.write(content)
        file.close()
