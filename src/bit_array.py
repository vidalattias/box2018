class BitArray:
    def __init__(self, _size):
        print("Creating an array of " + str(_size) + " elements")
        self.bytes = bytearray(_size//8+1)
        self.size = _size

    def retrieve_element(self, _index):
        if _index > self.size:
            raise IndexError
        else:
            q = _index // 8
            r = _index % 8

            working_byte = self.bytes[q]
            bit = (working_byte//(2**(r)))%2

            return bit

    def set_element(self, _index, _value):
        if _index > self.size:
            raise IndexError
        else:
            q = _index // 8
            r = _index % 8

            working_byte = self.bytes[q]
            bit = (working_byte//(2**(r)))%2

            if _value == 1 and bit == 0:
                self.bytes[q] += 2**r
            if _value == 0 and bit == 1:
                self.bytes[q] -= 2**r
