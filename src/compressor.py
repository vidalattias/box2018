from bloom_filter import BloomFilter
import pickle
import math
from collections import Counter
import matplotlib.pyplot as plt
import itertools
from progressbar import ProgressBar
import time

class Compressor:
    def __init__(self, file_name, filter_size, kmer_size, kmer_threshold):
        self.reads = []

        file = open("fasta/"+file_name+".fasta")
        for line in file:
            if line[0] != ">":
                self.reads.append(line.replace("\n",""))

        self.nb_reads = len(self.reads)
        self.file_name = file_name
        self.filter_size = filter_size
        self.kmer_size = kmer_size
        self.kmer_threshold = kmer_threshold
        self.filter = BloomFilter(filter_size)
        self.filter_construct_time = 0
        self.compression_time = 0


    def read_to_set(self, read):
        kmer_set = []
        L = len(read)
        k = self.kmer_size

        for i in range(L-k+1):
            kmer_set.append(read[i:i+k])

        return kmer_set

    def count_kmer(self):
        print("Starting to kmerize the reads")
        t1 = time.time()
        self.kmer_total_set = []

        j = 0
        for read in self.reads:
            j += 1
            progress = ProgressBar(self.nb_reads)
            progress.print(j)
            self.kmer_total_set += self.read_to_set(read)

        print("")

        print("Counting the kmers")
        self.kmer_counter = Counter(self.kmer_total_set)
        self.trace_histogram()
        print("Constructing the Bloom filter")
        uniq_set = set(self.kmer_total_set)
        uniq_set = [x for x in set(self.kmer_total_set) if self.kmer_counter[x] >= self.kmer_threshold]


        any(self.filter.add_word(kmer) for kmer in uniq_set)
        self.filter_construct_time = time.time()-t1
        print("Construct time : "+str(self.filter_construct_time))


    def trace_histogram(self):
        counter_of_counter = Counter(self.kmer_counter.values())

        x = counter_of_counter.keys()
        y = counter_of_counter.values()

        lists = sorted(zip(*[x, y]))
        new_x, new_y = list(zip(*lists))
        plt.plot(counter_of_counter.keys(), counter_of_counter.values(),'ro')
        #plt.show()
        plt.savefig(self.file_name+"_"+str(self.kmer_size)+".png")

    def consume(self, length):
        n = len(self.read)
        return_value = self.read[:length]
        self.read = self.read[length:]

        return return_value

    def next_character(self, position, next_char):
        k = self.kmer_size

        characters = []
        if self.filter.request(self.read[position:position+k-1]+"A"):
            characters += ["A"]
        if self.filter.request(self.read[position:position+k-1]+"C"):
            characters += ["C"]
        if self.filter.request(self.read[position:position+k-1]+"G"):
            characters += ["G"]
        if self.filter.request(self.read[position:position+k-1]+"T"):
            characters += ["T"]

        assert(self.read[position:position+k-1] != "")

        if len(characters) >= 2:
            if next_char in characters:
                return next_char
            else:
                return "E"
        if len(characters) == 1:
            if next_char in characters:
                return ""
            else:
                return "E"

        if len(characters) == 0:
            return "E"

    def compress(self):
        print("We have to compress now")
        t1 = time.time()
        k = self.kmer_size
        l = len(self.reads[0])
        display_string = ""


        j = 0
        number_reads = len(self.reads)

        progress = ProgressBar(self.nb_reads)

        for read in self.reads:
            temp_display = read[:k]

            self.read = read
            j += 1

            erroneous_read = False
            position = 0
            l = len(read)
            self.consume(1)
            for position in range(l-k):
                next = self.next_character(position, read[position+k])
                if next == "E":
                    erroneous_read = True
                    break
                else:
                    temp_display += next

            if erroneous_read == True:
                display_string += "@"+read
            else:
                display_string += temp_display
            progress.print(j)
        print("")

        self.compression_time = time.time()-t1
        print("compression_time : "+str(self.compression_time))

        file = open(self.file_name +"_compression.txt", 'a')
        file.write( str(self.kmer_size)+";"+str(self.filter_size)+";"+str(self.kmer_threshold)+";"+str(self.filter_construct_time)+";"+str(self.compression_time)+"\n")
        file.close()


        print('ON ECRIT DANS '+"result/"+self.file_name+"."+str(self.kmer_size)+"."+str(self.filter_size)+"."+str(self.kmer_threshold)+".comp")
        file = open("result/"+self.file_name+"."+str(self.kmer_size)+"."+str(self.filter_size)+"."+str(self.kmer_threshold)+".comp","w")
        file.write(display_string)

        pickle.dump({'filter':self.filter,'kmer_size':self.kmer_size,'nb_reads':self.nb_reads,'read_size':l}, open("result/"+self.file_name+"."+str(self.kmer_size)+"."+str(self.filter_size)+"."+str(self.kmer_threshold)+".graph.pgz","wb"))
