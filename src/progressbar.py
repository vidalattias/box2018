class ProgressBar:
    def __init__(self, limit, size=100):
        self.size = size
        self.limit = limit

    def print(self, value):
        size = self.size
        ratio = value/self.limit

        output = "|"+'█'*int(ratio*size)+' '*int((1-ratio)*size)+"| "+str(int(ratio*100))+"% complete"

        print(output, end = "\r")
